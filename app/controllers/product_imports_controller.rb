class ProductImportsController < ApplicationController
  before_action :set_import_columns

  def new
    @product_import = ProductImport.new
  end

  def create
    @product_import = ProductImport.new(params[:product_import])
    if @product_import.save
      redirect_to admin_path, notice: 'Products sucessfully imported!'
    else
      render :new
    end
  end

  private

  def set_import_columns
    @import_columns = %w(title description price)
  end
end
