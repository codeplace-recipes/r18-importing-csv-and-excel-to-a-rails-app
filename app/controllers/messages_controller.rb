class MessagesController < ApplicationController
  http_basic_authenticate_with name: 'admin', password: 'secret', only: :destroy
  before_action :set_message, only: :destroy

  def new
    @message = Message.new
  end

  def create
    @message = Message.new(message_params)
    redirect_to contact_path if @message.save
  end

  def destroy
    redirect_to admin_path if @message.delete
  end

  private

  def set_message
    @message = Message.find(params[:id])
  end

  def message_params
    params.require(:message).permit(:name, :email, :body)
  end
end
