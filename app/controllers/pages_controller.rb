class PagesController < ApplicationController
  http_basic_authenticate_with name: 'admin', password: 'secret', only: :admin

  def home
    @latest = Product.latest
  end

  def admin
    @products = Product.all
    @messages = Message.all
  end
end
