class Product < ApplicationRecord
  validates :title, presence: true
  validates :price, presence: true
  validates_numericality_of :price, greater_than: 0.49, message: 'must be at least 50 cents'
  has_many :sales

  scope :latest, -> { last(4).reverse }
end
